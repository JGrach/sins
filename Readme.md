# SINS PROJECT

## INTRODUCE

### CONTEXT

We have just finish our studies and we know how to walk. We are curious, we need to travel.
This project is a train for us, we want to try for first use mocha, webrtc, some other stuffs and provide a skype-like but Sins Is Not Skype.
We are entirely open to listen advices and ideas to optimize our application.
May this exercise serve your needs.

## DEPENDENCIES

* Docker: https://www.docker.com/
* Docker Compose: https://docs.docker.com/compose/

## INSTALL

### UNIX SYSTEM

* ssh:

        git clone git@gitlab.com:JGrach/sins.git && cd sins && npm run up

* https:

        git clone https://gitlab.com/JGrach/sins.git && cd sins && npm run up

* access:
by default back api is running on localhost:8000


## ISSUES

## CREDITS
Ahmadou Gueye && Julien Grach