const bcrypt = require("../handlers/bcrypt"),
  { sign, blacklist } = require("../handlers/webToken"),
  errors = require("../handlers/errors"),
  usersModel = require("../models/users")

async function login(params){
  const { login, password } = params
  try{
    const user = await usersModel.showLogin(login)
    if(user.length == 0) throw errors.noReference(new Error(), "user login")
    
    const isAllow = await bcrypt.compare(password, user[0].password)
    if(!isAllow) throw errors.badPassword(new Error())

    delete user[0].password
    return { token: await sign(user[0]) }
  } catch(err) {
    return err
  }
}

async function subscribe(params){
  const { login, password } = params
  try{
    const user = {
      login,
      password: await bcrypt.hash(password),
      isAdmin: false
    }

    user.id = await usersModel.addUser(user)
    user.id = user.id[0]
    
    return {
      token: await sign(user),
    }

  } catch(err) {
    return err
  }
}

async function disconnect(params){
  const token = params.token
  try{
    await blacklist(token)
    return 
  } catch(err){
    return err
  }
}

module.exports = {
  login,
  subscribe,
  disconnect
}