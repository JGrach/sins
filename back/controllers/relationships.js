const errors = require("../handlers/errors"),
  relationsModel = require("../models/relationships"),
  { verify } = require("../handlers/webToken")

async function getRelations(params){
  const userID = params.userID || params.currentUser.id
  const { userIDTarget } = params

  try {
    if(!userIDTarget){
      return { relations: await relationsModel.showRelationships(userID) }
    }
    
    const relation = await relationsModel.showOneRelationship(userID, userIDTarget)
    if(relation.length == 0){
      throw errors.noReference(new Error(), "id user target")
    }
    return { relations: relation }
  } catch(err) {
    return err
  }
}

async function postRelation(params){
  const { userIDTarget } = params
  const userID = params.userID || params.currentUser.id

  try {
    await relationsModel.addRelationship(userID, userIDTarget)
    return 
  } catch(err) {
    return err
  }
}

async function delRelation(params){
  const { userIDTarget } = params
  const userID = params.userID || params.currentUser.id

  try{
    const relationExist = await relationsModel.showOneRelationship(userID, userIDTarget, true)
    if(relationExist.length == 0)
    throw errors.noReference(new Error(), "id user target")

    const relation = await relationsModel.delRelationship(userID, userIDTarget)

    return 
  } catch(err) {
    return err
  }
}

module.exports = {
  getRelations,
  postRelation,
  delRelation
}
