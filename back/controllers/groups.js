const errors = require("../handlers/errors"),
  groupsModel = require("../models/groups"),
  relationsModel = require("../models/relationships")
  
async function getGroups(params){
  const { groupID } = params
  const userID = params.userID || params.currentUser.id

  try {
    if(!groupID) 
      return { groups: await groupsModel.showGroups(userID) }
    
    const group = await groupsModel.showOneGroup(userID, groupID)
    if(group.length == 0)
      throw errors.noReference(new Error(), "id group")

    return { group }
  } catch(err) {
    return err
  }
}

async function postGroup(params){
  const { name, picto } = params
  const userID = params.userID || params.currentUser.id

  try{
    const groupsForUser = await groupsModel.showGroups(userID, false)
    const id = groupsForUser.length + 1
    await groupsModel.addGroup(userID, {id, name, picto})
    return 
  } catch(err) {
    return err
  }
}

async function putGroup(params){
  const { groupID, name, picto } = params
  const userID = params.userID || params.currentUser.id

  try{
    await groupsModel.updateGroup(userID, groupID, {name, picto})
    return 
  } catch(err) {
    return err
  }
}

async function postRelationToGroup(params){
  const { groupID, relationID } = params
  const userID = params.userID || params.currentUser.id

  try{
    const relation = await relationsModel.showOneRelationship(userID, relationID, true)
    if(relation.length == 0)
      throw errors.noReference(new Error(), "id relation")

    await groupsModel.addRelationToGroup(userID, relation[0], groupID)
    return 
  } catch(err){ 
    return err
  }
}

async function delRelationFromGroup(params){
  const { groupID, relationID } = params
  const userID = params.userID || params.currentUser.id

  try{
    const relationExist = await relationsModel.showOneRelationship(relationID, userID, true)
    if(relationExist.length == 0)
      throw errors.noReference(new Error(), "id relation")
    
    const groupExist = await groupsModel.showOneGroup(userID, groupID)
    if(groupExist.length == 0)
      throw errors.noReference(new Error(), "id group")

    await groupsModel.delRelationToGroup(userID, relationID, groupID)
    return 
  } catch(err){ 
    return err
  }
}

async function delGroup(params){
  const { groupID } = params
  const userID = params.userID || params.currentUser.id

  try{
    const groupExist = await groupsModel.showOneGroup(userID, groupID)
    if(groupExist.length == 0)
    throw errors.noReference(new Error(), "id group")

    await groupsModel.deleteGroup(userID, groupID)
    return 
  } catch(err){ 
    return err
  }
}

module.exports = {
  getGroups,
  postGroup,
  putGroup,
  postRelationToGroup,
  delRelationFromGroup,
  delGroup
}