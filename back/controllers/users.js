const bcrypt = require("../handlers/bcrypt"),
  { sign } = require("../handlers/webToken"),
  errors = require("../handlers/errors"),
  configsModel = require("../models/configs"),
  usersModel = require("../models/users"),
  authCtrl = require("./auth")
  
async function getUsers(params){
  const { userID } = params
  try{
    if(!userID) {
      return { users: await usersModel.showUsers() }
    }
    const user = await usersModel.showOneUser(userID)
    if(user.length == 0){
      throw errors.noReference(new Error(), "id user")
    }
    return { user }
  } catch(err) {
    return err
  }
}

async function getConfig(params){
  const userID = params.userID || params.currentUser.id
  try{
    const config = await configsModel.showConfig(userID)
    if(config.length == 0){
      throw errors.noReference(new Error(), "id user")
    }
    return { config }
  } catch(err) {
    return err
  }
}

async function putConfig(params){
  const { currentUser, userID = currentUser.id } = params 
  const config = { 
    picto: params.picto, 
  }
  
  try{
    if(currentUser.isAdmin) config.isAdmin = params.isAdmin
    else if(!currentUser.isAdmin && typeof params.isAdmin == "boolean")
      throw errors.notAdmin(new Error())

    await configsModel.updateConfig(userID, config)
    return 
  } catch(err) {
    return err
  }
}

async function putLogin(params){
  const { login, password, userID = params.currentUser.id } = params
  try{
    const hash = await bcrypt.hash(password)
    const newLogin = await usersModel.updateUser(userID, { login, password: hash })
    const newToken = await authCtrl.login({login, password})
    if(newToken instanceof Error) throw newToken

    return result = { 
      token: newToken.token
    }
   } catch(err) {
    return err
  }
}

async function delUser(params){
  const { userID = params.currentUser.id } = params
  try{
    await authCtrl.disconnect(params)
    await usersModel.deleteUser(userID)
    return 
  } catch(err) {
    return err
  }
}

module.exports = {
  getUsers,
  getConfig,
  putConfig,
  putLogin,
  delUser,
}