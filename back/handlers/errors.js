function missing(errInstance, data){
  errInstance.message = "Missing data for " + data
  errInstance.httpStatus = 400
  return errInstance
}

function badFormat(errInstance, data){
  errInstance.message = data ? "Send bad format for " + data : "Send bad format"
  errInstance.httpStatus = 400
  return errInstance
}

function noReference(errInstance, data){
  errInstance.message = "No reference for " + data
  errInstance.httpStatus = 400
  return errInstance
}

function notAdmin(errInstance){
  errInstance.message = "Must be admin"
  errInstance.httpStatus = 403
  return errInstance
}

function badPassword(errInstance){
  errInstance.message = "Password is wrong"
  errInstance.httpStatus = 403
  return errInstance
}

function badToken(errInstance){
  errInstance.message = "Token bad format"
  errInstance.httpStatus = 403
  return errInstance
}

function tokenBlacklist(errInstance){
  errInstance.message = "Token blacklisted"
  errInstance.httpStatus = 403
  return errInstance
}

function noToken(errInstance){
  errInstance.message = "Token was expected"
  errInstance.httpStatus = 403
  return errInstance
}

function notFound(errInstance, originalUrl){
  errInstance.message = "'" + originalUrl + "' not found"
  errInstance.httpStatus = 404
  return errInstance
}


module.exports = {
  notFound,
  noToken,
  tokenBlacklist,
  badToken,
  badPassword,
  badFormat,
  noReference,
  missing,
  notAdmin
}