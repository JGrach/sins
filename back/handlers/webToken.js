const jwt = require("jsonwebtoken"),
  redis = require('../configs/redis'),
  { promisify } = require('util')

const errors = require("./errors")

function extractBearerToken(bearerToken){
  if (typeof bearerToken !== "string") {
    return errors.badToken(new Error())
  }
  const matches = bearerToken.match(/(bearer)\s+(\S+)/i)
  if(matches && matches[2]) return matches[2]
  return errors.badToken(new Error())
}

function sign(content, expiration){
  const jwtOptions = {
    expiresIn: expiration || process.env.JWT_EXPIRATION
  }
  return promisify(jwt.sign)(content, process.env.JWT_SECRET, jwtOptions)
}

async function verify(token){
  token = extractBearerToken(token)
    try{  
      if( token instanceof Error ) throw token

      const blacklist = await promisify(redis.get).bind(redis)(token);

      if(blacklist == "blacklist") throw errors.tokenBlacklist(new Error())
      return promisify(jwt.verify)(token, process.env.JWT_SECRET) 
    } catch(err) {
      return Promise.reject(err)
    }
}

async function blacklist(token){
  token = extractBearerToken(token)
  try{  
    if( token instanceof Error ) throw token
    const settedBlacklist = await promisify(redis.set).bind(redis)(token, "blacklist")
    await promisify(redis.expire).bind(redis)(token, 1 * 60 * 60 * 24 * 7)
    return settedBlacklist
  } catch(err) {
    return Promise.reject(err)
  }
}

module.exports = {
  sign,
  verify,
  blacklist,
}