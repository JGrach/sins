const bcrypt = require("bcrypt")

const saltRounds = (process.env.BCRYPT_ROUND || (() => {
    console.error("\x1b[33m", "Env variable BCRYPT_ROUND no detected, set to " + 10, "\x1b[0m")
    return 10
  })())

function hash(password){
  return bcrypt.hash(password, saltRounds)
}

function compare(password, hashToCompare){
  return bcrypt.compare(password, hashToCompare)
}

module.exports = {
  hash,
  compare
}