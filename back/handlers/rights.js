const { verify } = require("./webToken"),
  errors = require("./errors")

async function tokenToCurrentUser(req, res, next){
  const token = req.headers.authorization
  if (!token) return next()
  try{  
    req.currentUser = await verify(token)
    next()
  } catch(err) {
    next(err)
  }
}

function shouldBeConnected(req, res, next){
  if(!req.currentUser) return next(errors.noToken(new Error()))
  next()
}

function shouldBeAdmin(req, res, next){
  if(!req.currentUser) return next(errors.noToken(new Error()))
  if(!req.currentUser.isAdmin) return next(errors.notAdmin(new Error()))
  next()
}

module.exports = { tokenToCurrentUser, shouldBeConnected, shouldBeAdmin }
