const errors = require("./errors")

function httpHdlr(fn){
  return async function(req, res, next){
    const args = Object.assign(
      req.params, 
      req.body, 
      req.query, 
      { currentUser: req.currentUser }, 
      { token: req.headers.authorization }
    )
    const results = await fn(args)
    if(results instanceof Error) return next(results)

    const response = Object.assign({ state: "success" }, results)
    res.status(200).json(response)
  }
}

function resourceNotFound(req, res, next){
  next(errors.notFound(new Error(), req.originalUrl))
}

function errorHdlr(err, req, res, next){
  if(process.env.NODE_ENV !== "test" || process.env.LOG_ERR == "true") console.error(err)
  const response = { state: "error", error: err.message }
  res.status(err.httpStatus || 500).json(response)
}

module.exports = {
  httpHdlr,
  errorHdlr,
  resourceNotFound
}