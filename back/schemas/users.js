const Joi = require("joi")

const create = {
  body: Joi.object().keys({
    login: Joi.string().min(3).max(45).required(),
    password: Joi.string().min(3).max(45).required(),
  })
}


const target = {
  params: Joi.object().keys({
    userID: Joi.number().integer().min(1)
  })
} 

module.exports = {
  create,
  update: create,
  login: create,
  target
}

