const Joi = require("joi")

const update = {
  body: Joi.object().keys({
    picto: Joi.string().max(245).regex(/img\/picto\/generics|uploads\/\w+.png|jpg/),
    isAdmin: Joi.boolean()
  })
}

module.exports = {
  update
}

