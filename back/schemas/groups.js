const Joi = require("joi")

const create = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(45).required(),
    picto: Joi.string().max(245).regex(/img\/picto\/generics|uploads\/\w+.png|jpg/),
  })
}

const update = {
  params: Joi.object().keys({
    groupID: Joi.number().integer().min(1).required()
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(45),
    picto: Joi.string().max(245).regex(/img\/picto\/generics|uploads\/\w+.png|jpg/),
  })
}

const target = {
  params: Joi.object().keys({
    groupID: Joi.number().integer().min(1)
  })
}

module.exports = {
  create,
  update,
  target
}

