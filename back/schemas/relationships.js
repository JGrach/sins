const Joi = require("joi")

const target = {
  params: Joi.object().keys({
    userIDTarget: Joi.number().integer().min(1)
  })
}

module.exports = {
  create: target,
  target
}

