const knex = require("./knex")

function relationships(){
  return knex("relationships")
}

function aggregationGroup(relations){

  if(relations.length == 0) return relations

  function changeGroup(relation, groups = []){
    if(relation.groupID != null){
      groups.push ({ 
        groupID: relation.groupID, 
        groupName: relation.groupName, 
        groupPicto: relation.groupPicto 
      })
    }
    return groups
  }

  function sortRelationsByID(relations){
    return relations.reduce( (relationSorted, relation) => {
      let relationExist = relationSorted.find( sorted => sorted.relationID == relation.relationID)

      if(!relationExist) {
        relationExist = {
          relationID: relation.relationID,
          relationLogin: relation.relationLogin,
          relationPicto: relation.relationPicto,
          groups: []
        }
        relationSorted.push(relationExist)
      }
      
      relationExist.groups = changeGroup(relation, relationExist.groups)
      return relationSorted
    }, [])
  }

  return sortRelationsByID(relations)
}

function showRelationships(userID){
  return relationships().select(
    "users.id as relationID", "users.login as relationLogin", "configs.picto as relationPicto", 
    "groups.id as groupID", "groups.name as groupName", "groups.picto as groupPicto"
  )
  .leftJoin("group_has_relationships", function(){
    this.on("group_has_relationships.relationships_user_id1", "=", "relationships.user_id1")
    .andOn("group_has_relationships.relationships_user_id2", "=", "relationships.user_id2")
  })
  .leftJoin("groups", function(){
    this.on("group_has_relationships.group_user_id", "=", "groups.user_id")
    .andOn("group_has_relationships.group_id", "=", "groups.id")
  })
  .innerJoin("users", function(){
    this.on("relationships.user_id1", "=", "users.id").andOn("relationships.user_id1", "!=", parseInt(userID))
      .orOn("relationships.user_id2", "=", "users.id").andOn("relationships.user_id2", "!=", parseInt(userID))
  })
  .innerJoin("configs", "configs.user_id", "users.id")
  .where({"relationships.user_id1": userID})
  .orWhere({"relationships.user_id2": userID})
  .then( res => aggregationGroup(res))
}

function showOneRelationship(userID, relationID, minimal = false){
  let select;
  if(minimal) {
    select = ["relationships.*", "groups.id as groupID"]
  } else {
    select = [
      "users.id as relationID", "users.login as relationLogin", "configs.picto as relationPicto", 
      "groups.id as groupID", "groups.name as groupName", "groups.picto as groupPicto"
    ]
  }
  return relationships().select(...select)
  .leftJoin("group_has_relationships", function(){
    this.on("group_has_relationships.relationships_user_id1", "=", "relationships.user_id1")
    .andOn("group_has_relationships.relationships_user_id2", "=", "relationships.user_id2")
  })
  .leftJoin("groups", function(){
    this.on("group_has_relationships.group_user_id", "=", "groups.user_id")
    .andOn("group_has_relationships.group_id", "=", "groups.id")
  })
  .innerJoin("users", function(){
    this.on("relationships.user_id1", "=", "users.id").andOn("relationships.user_id1", "!=", parseInt(userID))
      .orOn("relationships.user_id2", "=", "users.id").andOn("relationships.user_id2", "!=", parseInt(userID))
  })
  .innerJoin("configs", "configs.user_id", "users.id")
  .where({ user_id1: userID, user_id2: relationID })
  .orWhere({ user_id2: userID, user_id1: relationID })
  .then( res => minimal ? res : aggregationGroup(res))
}

function addRelationship(userID, relationID ){
  return relationships().insert({ user_id1: userID, user_id2: relationID })
}

function delRelationship(userID, relationID){
  return relationships().where({ user_id1: userID, user_id2: relationID }).del()
}

module.exports = {
  showRelationships,
  showOneRelationship,
  addRelationship,
  delRelationship
}