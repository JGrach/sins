const { hash } = require("../../../handlers/bcrypt")

const seeds = [
  {
    users: { login: "tom" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: { isAdmin: true },
  },
  {
    users: { login: "jerry" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: {},
  },
  {
    users: { login: "titi" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: {},
  },
  {
    users: { login: "grominet" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: { isAdmin: true },
  },
  {
    users: { login: "mickey" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: {},
  },
  {
    users: { login: "dingo" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: {},
  },
  {
    users: { login: "pluto" },
    passwords: hash("123").then(function(password){ return { password } }),
    configs: {},
  },
]

function deleteSeeds(knex, table){
  return knex(table).del()
}

function insertSeeds(knex, table, seeds){
  function query(){
    return knex(table).insert(seeds)
  }
  if(table == 'users') return query().returning('id')
  return query()
}

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return deleteSeeds(knex, "users")
  .then(function(){
    const inserts = seeds.map(function(seed){
      return insertSeeds(knex, 'users', seed.users).then(function(id){
        const pass = seed.passwords.then(function(pass){ 
          return insertSeeds(knex, 'passwords', Object.assign(pass, { user_id: id[0] }))
        })
        const configs = insertSeeds(knex, 'configs', Object.assign(seed.configs, { user_id: id[0] }))
        return Promise.all([pass, configs]).then( () => id[0] )
      })
    })
    return Promise.all(inserts)
  }).then( res => {
    const relation = [
      {
        user_id1 : res[0],
        user_id2 : res[1]
      },
      {
        user_id1 : res[0],
        user_id2 : res[2]
      },
      {
        user_id1 : res[0],
        user_id2 : res[3]
      },
      {
        user_id1 : res[1],
        user_id2 : res[2]
      },
      {
        user_id1 : res[1],
        user_id2 : res[3]
      },
      {
        user_id1 : res[2],
        user_id2 : res[3]
      },
      {
        user_id1 : res[4],
        user_id2 : res[5]
      },
      {
        user_id1 : res[4],
        user_id2 : res[6]
      },
      {
        user_id1 : res[5],
        user_id2 : res[6]
      },
    ]
    return knex('relationships').insert(relation).then(() => res)
  })
  .then((res) => {
    const group = [
      {
        user_id: res[0],
        id: 1,
        name: "friends"
      },
      {
        user_id: res[0],
        id: 2,
        name: "ennemies"
      },
      {
        user_id: res[3],
        id: 1,
        name: "nourritures"
      },
    ]
    return knex('groups').insert(group).then(() => res)
  })
  .then((res) => {
    const group = [
      {
        group_user_id: res[0],
        group_id: 1,
        relationships_user_id1: res[0],
        relationships_user_id2: res[3]
      }, 
      {
        group_user_id: res[0],
        group_id: 2,
        relationships_user_id1: res[0],
        relationships_user_id2: res[1]
      }, 
      {
        group_user_id: res[0],
        group_id: 2,
        relationships_user_id1: res[0],
        relationships_user_id2: res[2]
      },
      {
        group_user_id: res[3],
        group_id: 1,
        relationships_user_id1: res[1],
        relationships_user_id2: res[3]
      },
      {
        group_user_id: res[3],
        group_id: 1,
        relationships_user_id1: res[2],
        relationships_user_id2: res[3]
      }
    ]
    return knex('group_has_relationships').insert(group).then(() => res)
  })
  .catch(function(err){
    console.log(err)
  })
};
