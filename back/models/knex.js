let config = require("../configs/knexfile.js")[process.env.NODE_ENV];

module.exports = require("knex")(config);