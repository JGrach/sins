exports.up = function(knex, Promise) {
  return knex.schema.createTable("configs", table => {
    table.integer("user_id").primary().unsigned().unique().notNullable()
    table.boolean("isAdmin").notNullable().defaultTo(false)
    table.string("picto").notNullable().defaultTo("img/picto/generic/profilPicto.jpg")

    table.foreign("user_id").references("users.id").onUpdate("CASCADE").onDelete("CASCADE")
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("configs")
}
