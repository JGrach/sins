exports.up = function(knex, Promise) {
  return knex.schema.createTable("users", table => {
    table.increments()
    table.timestamps(true, true)
    table.string("login").notNullable()
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("users")
}
