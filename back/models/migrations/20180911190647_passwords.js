exports.up = function(knex, Promise) {
  return knex.schema.createTable("passwords", table => {
    table.integer("user_id").primary().unsigned().unique().notNullable()
    table.string("password").notNullable()

    table.foreign("user_id").references("users.id").onUpdate("CASCADE").onDelete("CASCADE")
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("passwords")
}
