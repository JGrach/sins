exports.up = function(knex, Promise) {
  return knex.schema.createTable("group_has_relationships", table => {
    table.integer("group_user_id").unsigned().notNullable()
    table.integer("group_id").unsigned().notNullable()
    table.integer("relationships_user_id1").unsigned().notNullable()
    table.integer("relationships_user_id2").unsigned().notNullable()

    table.foreign(["group_user_id", "group_id"]).references(["user_id", "id"])
      .inTable("groups").onUpdate("CASCADE").onDelete("CASCADE")
    table.foreign(["relationships_user_id1", "relationships_user_id2"]).references(["user_id1", "user_id2"])
      .inTable("relationships").onUpdate("CASCADE").onDelete("CASCADE")
    
    table.primary(["group_user_id", "group_id", "relationships_user_id1", "relationships_user_id2"])
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("group_has_relationships")
}

