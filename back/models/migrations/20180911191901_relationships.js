exports.up = function(knex, Promise) {
  return knex.schema.createTable("relationships", table => {
    table.integer("user_id1").unsigned().notNullable()
    table.integer("user_id2").unsigned().notNullable()
    
    table.foreign("user_id1").references("id")
      .inTable('users').onUpdate("CASCADE").onDelete("CASCADE")
    table.foreign("user_id2").references("id")
      .inTable('users').onUpdate("CASCADE").onDelete("CASCADE")

    table.primary(["user_id1", "user_id2"])
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("relationships")
}

