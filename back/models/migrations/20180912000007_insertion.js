const { hash } = require("../../handlers/bcrypt")

const user = [{
  login: process.env.FIRST_USER
}]

const config = [{
  user_id: 1,
  isAdmin: true,
}]

const password = new Promise((resolve, reject) => {
  hash(process.env.FIRST_USER_PASS).then( res => {
    resolve([{ user_id: 1, password: res }])
  }).catch((err) => {
    reject(err)
  })
})

exports.up = function(knex, Promise) {
  return knex.batchInsert("users", user).then(() => {
    return Promise.all([password, knex.batchInsert("configs", config)])
  }).then((res) => {
    return knex.batchInsert("passwords", res[0])
  })
};

exports.down = function(knex, Promise) {
  return knex.schema
};
