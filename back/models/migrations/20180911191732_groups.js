exports.up = function(knex, Promise) {
  return knex.schema.createTable("groups", table => {
    table.integer("user_id").unsigned().notNullable()
    table.integer("id").unsigned().notNullable()
    table.timestamps(true, true)
    table.string("name").notNullable()
    table.string("picto").notNullable().defaultTo("img/picto/generic/groupPicto.jpg")
    
    table.foreign("user_id").references("users.id").onUpdate("CASCADE").onDelete("CASCADE")
    table.primary(["user_id", "id"])
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable("groups")
}
