const knex = require("./knex")

function configs(){
  return knex("configs")
}

function showConfig(userID){
  return configs().where({ user_id: userID })
}

function updateConfig(userID, config){
  return configs().where({ user_id: userID }).update(config)
}

module.exports = {
  showConfig,
  updateConfig
}