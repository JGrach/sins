const knex = require("./knex")

function groups(){
  return knex("groups")
}

function aggregationGroup(groups){
  if(groups.length == 0) return groups
  const { groupID, groupPicto, name, updated_at, created_at } = groups[0]

  const relations = []
  
  groups.forEach( group => {
    if(group.relationID == null) return
    relations.push ({ 
      relationID: group.relationID, 
      relationLogin: group.relationLogin, 
      relationPicto: group.relationPicto 
    })
  })

  return [{ 
    groupID, 
    groupPicto, 
    name, 
    updated_at, 
    created_at,
    relations
   }]
}

function showGroups(userID, all = true){
  let select;
  if(all) select = ["id", "name", "created_at", "updated_at", "picto"]
  else select = ["id"]
  return groups().select(select)
    .where({user_id: userID})
}

function showOneGroup(userID, groupID){
  return groups().select(
    "groups.id as groupID", "groups.name", "groups.created_at", "groups.updated_at", "groups.picto as groupPicto",
    "users.id as relationID", "users.login as relationLogin", "configs.picto as relationPicto"
  )
  .leftJoin("group_has_relationships", function(){
    this.on("groups.user_id", "=", "group_has_relationships.group_user_id")
    .andOn("groups.id", "=", "group_has_relationships.group_id")
  })
  .leftJoin("relationships", function(){
    this.on("group_has_relationships.relationships_user_id1", "=", "relationships.user_id1")
    .andOn("group_has_relationships.relationships_user_id2", "=", "relationships.user_id2")
  })
  .leftJoin("users", function(){
    this.on("relationships.user_id1", "=", "users.id").andOn("relationships.user_id1", "!=", parseInt(userID))
      .orOn("relationships.user_id2", "=", "users.id").andOn("relationships.user_id2", "!=", parseInt(userID))
  })
  .leftJoin("configs", "users.id", "configs.user_id")
  .where("groups.user_id", userID)
  .andWhere("groups.id", groupID)
  .then( res => aggregationGroup(res) )
}

function addGroup(userID, group){
  return groups().insert(Object.assign({ user_id: userID }, group ))
}

function updateGroup(userID, groupID, group){
  function now(){
    const now = new Date()
    return `${now.getFullYear()}-${now.getMonth()}-${now.getDate()} `
        + `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`
  }

  group.updated_at = now()

  return groups().where({ user_id: userID, id: groupID })
    .update(group)
}

function addRelationToGroup(userID, relation, groupID){
  return knex("group_has_relationships").insert({
    relationships_user_id1: relation.user_id1,
    relationships_user_id2: relation.user_id2,
    group_user_id: userID,
    group_id: groupID
  })
}

function delRelationToGroup(userID, relationID, groupID){
  return knex("group_has_relationships").where({
    group_user_id: userID,
    group_id: groupID,
    relationships_user_id1: userID,
    relationships_user_id2: relationID
  })
  .orWhere({
    group_user_id: userID,
    group_id: groupID,
    relationships_user_id1: relationID,
    relationships_user_id2: userID
  }).del()
}

function deleteGroup(userID, groupID){
  return groups().where({ user_id: userID, id: groupID }).del()
}

module.exports = {
  showGroups,
  showOneGroup,
  addGroup, 
  updateGroup,
  addRelationToGroup, 
  delRelationToGroup,
  deleteGroup
}