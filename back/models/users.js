const knex = require("./knex")

function users(){
  return knex("users")
}

function showUsers(){
  return users()
  .select("id", "login", "created_at", "updated_at", "configs.picto")
  .innerJoin("configs", "id", "=", "configs.user_id")

}

function showOneUser(userID){
  return users()
  .select("login", "created_at", "updated_at", "configs.picto")
  .innerJoin("configs", "id", "=", "configs.user_id")
  .where({id: userID})
}

function showLogin(login){
  return users().select("id", "login", "passwords.password", "configs.isAdmin")
  .where({ login })
  .join('configs', 'id', '=', 'configs.user_id')
  .join('passwords', 'id', '=', 'passwords.user_id')
}

function addUser(user){
  return users().insert( { login: user.login } )
    .returning("id")
    .then( ([id]) => {
      return knex("configs").insert({user_id: id}).returning("user_id")
        .catch( (err) => { 
          users().where({id}).del()
          throw err 
        })
    })
    .then( ([id]) => {
      return knex("passwords").insert({ user_id: id, password: user.password }).returning("user_id")
      .catch( (err) => { 
        users().where({id}).del()
        throw err 
      })
    })
}

function updateUser(userID, user){
  function now(){
    const now = new Date()
    return `${now.getFullYear()}-${now.getMonth()}-${now.getDate()} `
        + `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`
  }

  user.updated_at = now()

  return users().where({ id: userID }) // !!! error subrequest !!!
    .update({ login: user.login })
    .then( res => {
      const passwordModif = knex("passwords").where({ user_id: userID })
      .update({ password: user.password })
      return Promise.all([res, passwordModif])
    }) 
}

function deleteUser(userID){
  return users().where({ id: userID }).del()
}

module.exports = {
  showUsers,
  showOneUser,
  showLogin,
  addUser,
  updateUser,
  deleteUser
}