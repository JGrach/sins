# MOCK REPOSITORY

It's a mock repo to work with front.

 ## mocked routes availables

 ### route without bearer token

- public/users.json
for the GET /public/users route

- public/user/2.json
for the GET /public/user/:id route
available from 2 to 8

- auth/login.json
for the POST /auth/login route with { login: , password: }

- auth/subscribe.json
for the POST /auth/subscribe route with { login: , password: }

- auth/disconnect.json
for the DELETE /auth/disconnect route

### route with bearer token

- user/config.json 
for the GET /user/config route

- user/put.json 
for the PUT /user route

- groups/index.json or just groups
for the GET /groups route

- groups/1.json
for the GET /groups/:id route
available from 1 to 2

- relations/index.json or just relations
for the GET /relations route

### generic route

Other routes will return a json like /genericSuccess.json
You find too 3 json to simulate error.

### other routes without specific output:
- PUT /user 
- DELETE /user

- POST /groups
- PUT /groups/:id
- DELETE /groups/:id

- POST /relations/:userTargetId/groups/:id
- DELETE /relations/:userTargetId/groups/:id

- POST /relations
