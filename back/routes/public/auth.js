const router = require("express").Router(),
  validate = require("express-validation")
const authCtrl = require("../../controllers/auth"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  userSchema = require("../../schemas/users")

router.post("/login", 
  validate(userSchema.login),
  httpHdlr(authCtrl.login)
)

router.post("/subscribe", 
  validate(userSchema.create),
  httpHdlr(authCtrl.subscribe)
)

router.delete("/disconnect", 
  httpHdlr(authCtrl.disconnect)
)

module.exports = router