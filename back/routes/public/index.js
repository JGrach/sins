const router = require("express").Router(),
  validate = require("express-validation")
const usersCtrl = require("../../controllers/users"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  userSchema = require("../../schemas/users")

router.get("/users?", 
  httpHdlr(usersCtrl.getUsers)
)

router.get("/users?/:userID", 
  validate(userSchema.target),
  httpHdlr(usersCtrl.getUsers)
)

module.exports = router
