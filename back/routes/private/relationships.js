const router = require("express").Router(),
  validate = require("express-validation")
const relationsCtrl = require("../../controllers/relationships"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  relationSchema = require("../../schemas/relationships")

router.get("/", 
  httpHdlr(relationsCtrl.getRelations)
)

router.post("/:userIDTarget", 
  validate(relationSchema.create),
  httpHdlr(relationsCtrl.postRelation)
)

router.delete("/:userIDTarget", 
  validate(relationSchema.target),
  httpHdlr(relationsCtrl.delRelation)
)

module.exports = router
