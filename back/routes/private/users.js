const router = require("express").Router(),
  validate = require("express-validation")
const usersCtrl = require("../../controllers/users"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  userSchema = require("../../schemas/users"),
  configSchema = require("../../schemas/configs")
  
router.get("/config", 
  httpHdlr(usersCtrl.getConfig)
)

router.put("/", 
  validate(userSchema.update),
  httpHdlr(usersCtrl.putLogin)
)

router.put("/config", 
  validate(configSchema.update),
  httpHdlr(usersCtrl.putConfig)
)

router.delete("/", 
  httpHdlr(usersCtrl.delUser)
)

module.exports = router
