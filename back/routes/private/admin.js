const router = require("express").Router(),
  validate = require("express-validation")
const usersCtrl = require("../../controllers/users"),
  groupsCtrl = require("../../controllers/groups"),
  relationsCtrl = require("../../controllers/relationships"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  userSchema = require("../../schemas/users"),
  groupSchema = require("../../schemas/groups"),
  configSchema = require("../../schemas/configs")
  
router.get("/user/:userID/config", 
  httpHdlr(usersCtrl.getConfig)
)

router.get("/relation/user/:userID", 
  httpHdlr(relationsCtrl.getRelations)
)

router.get("/group/user/:userID", 
  httpHdlr(groupsCtrl.getGroups)
)

router.get("/group/:groupID/user/:userID", 
  validate(groupSchema.target),
  httpHdlr(groupsCtrl.getGroups)
)

router.put("/user/:userID", 
  validate(userSchema.update),
  httpHdlr(usersCtrl.putLogin)
)

router.put("/user/:userID/config", 
  validate(configSchema.update),
  httpHdlr(usersCtrl.putConfig)
)

router.delete("/user/:userID", 
  httpHdlr(usersCtrl.delUser)
)

module.exports = router
