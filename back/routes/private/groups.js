const router = require("express").Router(),
  validate = require("express-validation")
const groupsCtrl = require("../../controllers/groups"),
  { httpHdlr } = require("../../handlers/httpHandler"),
  groupSchema = require("../../schemas/groups"),
  relationSchema = require("../../schemas/relationships")

router.get("/", 
  httpHdlr(groupsCtrl.getGroups)
)

router.get("/:groupID", 
  validate(groupSchema.target),
  httpHdlr(groupsCtrl.getGroups)
)

router.post("/", 
  validate(groupSchema.create),
  httpHdlr(groupsCtrl.postGroup)
)

router.put("/:groupID", 
  validate(groupSchema.update),
  httpHdlr(groupsCtrl.putGroup)
)
router.delete("/:groupID", 
  validate(groupSchema.target),
  httpHdlr(groupsCtrl.delGroup)
)

router.post("/:groupID/relation/:relationID", 
  validate(Object.assign(groupSchema.target, relationSchema.target)),
  httpHdlr(groupsCtrl.postRelationToGroup)
)
router.delete("/:groupID/relation/:relationID",
  validate(Object.assign(groupSchema.target, relationSchema.target)), 
  httpHdlr(groupsCtrl.delRelationFromGroup)
)

module.exports = router
