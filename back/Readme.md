# Backend For Sins Project

## INSTALL

### UNIX SYSTEM

#### First Steps
This install protocol follow steps describe at: https://gitlab.com/JGrach/sins/

#### Migration
After launch database with docker-compose, you have to hydrate it with migration.

    npm run init

#### Launch
Now, you can launch server with:

    npm start

## OPTIONS

### config.json
Found this in /configs/config.json
Handle all url's config for app and webservices dependencies

      {
            "version": // Version number for API, include in the url
            "development": // a NODE_ENV possible value to specify wich config use on wich env
                  {
                        "database": // keys are webservices to specify where api can found it
                              {
                                    "host": "localhost",
                                    "port": 5432
                              }
                  }
      }

### environment

### npm commands
* Re-init database and launch with nodemon:

      npm run dev

* Launch with simple node:

      npm start

* Initialized or Re-Initialized database:

      npm run init

* Launch Mocha test:

    npm run test

* Delete all schemas of database:

      npm run rollback

* Add all schemas of database:

      npm run migrate

## ROUTES

All routes have prefix /v0 to api version indication.

### public - access without token

#### auth

#### index

### private - access with token

#### users

#### groups

#### relationships

## MOCKS

## ISSUES

## CREDITS
Ahmadou Gueye && Julien Grach