const env = require("./env")

function KnexConfig(){
  this[env] = {
    client: "pg",
    connection: {
      host: process.env.POSTGRES_HOST,
      port: process.env.POSTGRES_PORT,
      database: process.env.POSTGRES_DB,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD
    },
    migrations: {
      directory: __dirname + "/../models/migrations",
    },
    seeds: {
      directory: __dirname + "/../models/seeds/" + env,
    },
  }
  return this
}

module.exports = new KnexConfig
