const r = require('redis'),
  client = r.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  })

if(process.env.NODE_ENV !== "development") client.auth(process.env.REDIS_PASS)

module.exports = client
