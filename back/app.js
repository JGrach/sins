require("./configs/env")

const express = require("express"),
  app = express(),
  helmet = require("helmet"),
  multer  = require("multer")({ dest: "./static/img/uploads/" }),
  morgan = require('morgan')

const authRouter = require("./routes/public/auth"),
  publicRouter = require("./routes/public/index"),
  userRouter = require("./routes/private/users"),
  groupRouter = require("./routes/private/groups"),
  relationRouter = require("./routes/private/relationships"),
  adminRouter = require("./routes/private/admin"),
  { tokenToCurrentUser, shouldBeConnected, shouldBeAdmin } = require("./handlers/rights"),
  { resourceNotFound, errorHdlr } = require("./handlers/httpHandler"),
  v = "/v" + process.env.VERSION

if(process.env.NODE_ENV !== "test" && process.env.LOG_MORGAN == "true"){
  app.use((() => {
    const morganConf = process.env.LOG_CONF || "dev"
    return morgan(morganConf)
  })())
}

app.use(helmet())
app.use(express.static("./static"))
app.use(express.json())
app.use(multer.single("picto"))

app.use(tokenToCurrentUser)

app.use(v + "/auth", authRouter)
app.use(v + "/public", publicRouter)

app.use(v + "/users?", shouldBeConnected, userRouter)
app.use(v + "/groups?", shouldBeConnected, groupRouter)
app.use(v + "/relations?", shouldBeConnected, relationRouter)

app.use(v + "/admin", shouldBeAdmin, adminRouter)

app.use('*', resourceNotFound)
app.use(errorHdlr)

const server = app.listen(process.env.BACK_PORT, () => {
  console.log("server ready, listening on " + process.env.BACK_PORT)
})


// Using by mocha
function stop(){
  server.close()
}

module.exports = {
  app,
  stop
}