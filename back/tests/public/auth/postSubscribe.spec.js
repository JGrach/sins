const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { verify } = require("../../../handlers/webToken"),
  { compare } = require("../../../handlers/bcrypt"),
  { success } = require("../../handlers/genericResponse")

describe("Public Routes - Authentication - POST /subscribe", () => {
  const sender = new Sender("/auth/subscribe")

  it("should return grosminet's token", (done) => {
    const newUser = "grosminet"
    const newPass = "123!%*$¨321"
    const test = sender.body({login: newUser, password: newPass }).post()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("token")
        const token = res.body.token
        expect(token).a("string")
        return verify("bearer " + token).then( user => {
          expect(Object.keys(user)).includes("id", "login", "isAdmin")
          expect(user.id).a("number")
          expect(user.login).equal(newUser)
          expect(user.isAdmin).equal(false)
        })
      })

    expect(test).eventually.notify(done)
  })

  it("should create grosminet user", (done) => {
    const newUser = "grosminet"
    const newPass = "123!%*$¨321"
    const test = expect(queries.usersName()).eventually.members(["tom", "jerry", "titi"])
      .then( () => sender.body({login: newUser, password: newPass }).post() )
      .then( () => expect(queries.usersName()).eventually.members(["tom", "jerry", "titi", newUser]))

    expect(test).eventually.notify(done)
  })

  it("should create grosminet's password", (done) => {
    const newUser = "grosminet"
    const newPass = "123!%*$¨321"
    const test = expect(queries.passwordOfUser(newUser)).rejectedWith("Cannot read property 'password' of undefined")
      .then( () => sender.body({login: newUser, password: newPass }).post() )
      .then( () => queries.passwordOfUser(newUser) )
      .then( hash => compare(newPass, hash) )

    expect(test).eventually.notify(done)
  })

  it("should create grosminet's config", (done) => {
    const newUser = "grosminet"
    const newPass = "123!%*$¨321"
    const test = expect(queries.configsOfUser(newUser)).eventually.length(0)
      .then( () => sender.body({login: newUser, password: newPass }).post() )
      .then( () => queries.configsOfUser(newUser) )
      .then( configs => {
        expect(configs).length(1)
        expect(Object.keys(configs[0])).members([
          "user_id",
          "isAdmin",
          "picto"
        ])
        expect(configs[0].isAdmin).equal(false)
      })

    expect(test).eventually.notify(done)
  })
})