const { Sender, expect } = require("../../api"),
  { verify } = require("../../../handlers/webToken"),
  { success } = require("../../handlers/genericResponse")

describe("Public Routes - Authentication - POST /login", () => {
  const sender = new Sender("/auth/login")

  it("should return tom's token", (done) => {
    const myUser = "tom"
    const myPass = "123"

    const test = sender.body({ login: myUser, password: myPass}).post()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("token")
        const token = res.body.token
        expect(token).a("string")
        return verify("bearer " + token).then( user => {
          expect(Object.keys(user)).includes("id", "login", "isAdmin")
          expect(user.id).a("number")
          expect(user.login).equal(myUser)
          expect(user.isAdmin).equal(true)
        })
      })

    expect(test).eventually.notify(done)
  })
})




