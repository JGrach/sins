const { Sender, expect } = require("../../api"),
  { verify } = require("../../../handlers/webToken"),
  { success } = require("../../handlers/genericResponse")

describe("Public Routes - Authentication - DELETE /disconnect", () => {
  const sender = new Sender("/auth/disconnect")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const test = senderFor("tom").delete()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should blacklist tom's token", (done) => {
    const test = senderFor("tom").delete()
    .then( () => sender.getToken() )
      .then((token) => {
        return expect(verify("bearer " + token)).eventually.rejectedWith("Token blacklisted")
      })
    
    expect(test).eventually.notify(done)
  })
})