const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Public Routes - Index - GET /user/:userID", () => {
  const sender = new Sender("/public/user/:userID")

  it("should return success", (done) => {
    const test = queries.userID("tom")
      .then( id => sender.replaceUrl({":userID": id}).get() )
      .then( success )

    expect(test).eventually.notify(done)
  })

  it("should return user tom", (done) => {
    const myUser = "tom"
    const test = queries.userID(myUser)
      .then( id => sender.replaceUrl({":userID": id}).get() )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("user")
        const user = res.body.user[0]
        expect(user).a("object")
        expect(user).keys("login", "created_at", "updated_at", "picto")

        expect(user.login).equal(myUser)
        expect(user.created_at).a("string")
        expect(user.updated_at).a("string")
        expect(user.picto).equal("img/picto/generic/profilPicto.jpg")
      })

      expect(test).eventually.notify(done)
  })
})