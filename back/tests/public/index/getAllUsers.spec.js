const { Sender, expect } = require("../../api"),
  { success } = require("../../handlers/genericResponse")

describe("Public Routes - Index - GET /users", () => {
  const sender = new Sender("/public/users")

  it("should return success", (done) => {
    const test = sender.get()
    .then( success )
    
    expect(test).eventually.notify(done)
  })

  it("should return all users", (done) => {
    const test = sender.get()
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("users")
        const users = res.body.users
        expect(users).a("array").lengthOf(3)

        const logins = users.map(user => {
          expect(user).a("object")
          expect(user).keys("id", "login", "created_at", "updated_at", "picto")

          expect(user.login).a("string")
          expect(user.created_at).a("string")
          expect(user.updated_at).a("string")
          expect(user.picto).equal("img/picto/generic/profilPicto.jpg")

          return user.login
        });

        expect(logins).members(
          ["tom",
          "jerry",
          "titi"
        ])
      })
    expect(test).eventually.notify(done)
  })
})