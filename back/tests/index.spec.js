/* First level in directory tree, first readding file */
const queries = require("./handlers/queries"),
  { stop } = require("../app")

beforeEach(queries.knexSeed)

after(() => stop())