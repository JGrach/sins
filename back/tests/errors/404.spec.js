const { Sender, expect } = require("../api"),
  { error } = require("../handlers/genericResponse")

describe("Errors - 404 - GET Inexistent", () => {
  const sender = new Sender("/tata")

  it("should return error", (done) => {
    const test = sender.get()
      .then( error )

    expect(test).eventually.notify(done)
  })

  it("should return an explicit error not found", (done) => {
    const test = sender.get()
      .then( res => {
        expect(res.status).equal(404)
        const error = res.body.error
        expect(error).a("string")
        expect(error).equal("'/v0/tata' not found")
      })
    expect(test).eventually.notify(done)
  })
})