const { Sender, expect } = require("../api"),
  { error } = require("../handlers/genericResponse")

describe("Errors - 403 - GET /user/config", () => {
  const sender = new Sender("/user/config")

  it("should return error", (done) => {
    const test = sender.get()
      .then( error )

    expect(test).eventually.notify(done)
  })

  it("should return an explicit error not authorized", (done) => {
    const test = sender.get()
      .then( res => {
        expect(res.status).equal(403)
        const error = res.body.error
        expect(error).a("string")
        expect(error).equal("Token was expected")
      })
    expect(test).eventually.notify(done)
  })
})