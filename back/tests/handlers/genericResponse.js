const { expect } = require("chai")

function success(res){
  expect(res.status).equal(200)
  expect(res).property("body")
  expect(Object.keys(res.body)).includes("state")

  expect(res.body.state).equal("success")
  return res
}

function error(res){
  expect(res.status).within(400, 499)
  expect(res).property("body")
  expect(res.body).all.keys("state", "error")
  expect(Object.keys(res.body)).lengthOf(2)

  expect(res.body.state).equal("error")
  return res
}

module.exports = {
  success,
  error
}