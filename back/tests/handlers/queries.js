require("../../configs/env")

knex = require("../../models/knex")

function knexSeed(){
  return knex.seed.run()
}

function userID(login){
  return knex("users").select("id").where({ login }).then( user => user[0].id )
}

function usersName(){
  return knex("users").then(res => res.map( user => user.login))
}

function contentToken(login){
  return knex("users").select("id", "login", "configs.isAdmin")
  .where({ login })
  .join('configs', 'id', '=', 'configs.user_id')
}

function passwordOfUser(login){
  return knex("passwords")
  .select("passwords.password")
  .innerJoin("users"," users.id","=", "passwords.user_id")
  .where({ "users.login": login})
  .then( res => {
    return res[0].password
  })
}

function passwordById(id){
  return knex("passwords")
  .where({ "user_id": id})
}

function configById(id){
  return knex("configs")
  .where({"user_id": id})
}

function configsOfUser(login){
  return knex("configs")
  .select("configs.*")
  .innerJoin("users"," users.id","=", "configs.user_id")
  .where({ "users.login": login})
}

function loginOfUser(login){
  return knex("passwords")
  .select("users.id", "users.login", "passwords.password")
  .innerJoin("users"," users.id","=", "passwords.user_id")
  .where({ "users.login": login})
  .then( res => {
    return res[0]
  })
}

function relationsOfUser(login){
  return knex("relationships")
  .select("relations.login")
  .innerJoin("users", function(){
    this.on("relationships.user_id1", "=", "users.id").andOn("users.login", "=", knex.raw('?', [login]))
      .orOn("relationships.user_id2", "=", "users.id").andOn("users.login", "=", knex.raw('?', [login]))
  })
  .innerJoin("users as relations", function(){
    this.on("relationships.user_id1", "=", "relations.id").andOn("relations.login", "!=", knex.raw('?', [login]))
      .orOn("relationships.user_id2", "=", "relations.id").andOn("relations.login", "!=", knex.raw('?', [login]))
  })
}

function groupsOfUser(login){
  return knex("users")
  .select("groups.*")
  .innerJoin("groups", "users.id", "=", "groups.user_id")
  .where({ "users.login": login})
}

function groupOfUser(login, groupId){
  return knex("users").select(
    "groups.user_id as groupOwner", "groups.id as groupID", "groups.name",
    "relation.login as relationLogin"
  )
  .innerJoin("groups", function(){
    this.on("groups.user_id", "=", "users.id")
    .andOn("groups.id", "=", groupId)
  })
  .leftJoin("group_has_relationships", function(){
    this.on("group_has_relationships.group_id", "=", "groups.id")
    .andOn("group_has_relationships.group_user_id", "=", "groups.user_id")
  })
  .leftJoin("relationships", function(){
    this.on("group_has_relationships.relationships_user_id1", "=", "relationships.user_id1")
    .andOn("group_has_relationships.relationships_user_id2", "=", "relationships.user_id2")
  })
  .leftJoin("users as relation", function(){
    this.on("relationships.user_id1", "=", "relation.id").andOn("relation.login", "!=", knex.raw('?', [login]))
      .orOn("relationships.user_id2", "=", "relation.id").andOn("relation.login", "!=", knex.raw('?', [login]))
  })
  .where("users.login", login)
}

function groupDebug(ownerID){
  knex("groups")
    .select("groups.id", "groups.user_id", "groups.name", "relationships.user_id1", "relationships.user_id2", "relation.login as relationLogin", "owner.login as ownerLogin")
    .leftJoin("group_has_relationships", function(){
      this.on("groups.user_id", "=", "group_has_relationships.group_user_id")
      .andOn("groups.id", "=", "group_has_relationships.group_id")
    })
    .leftJoin("relationships", function(){
      this.on("group_has_relationships.relationships_user_id1", "=", "relationships.user_id1")
      .andOn("group_has_relationships.relationships_user_id2", "=", "relationships.user_id2")
    })
    .leftJoin("users as owner", "groups.user_id", "=", "owner.id")
    .leftJoin("users as relation", function(){
      this.on("relationships.user_id1", "=", "relation.id").andOn("relationships.user_id1", "!=", "owner.id")
        .orOn("relationships.user_id2", "=", "relation.id").andOn("relationships.user_id2", "!=", "owner.id")
    })
    .where("groups.user_id", ownerID)
    .andWhere("groups.id", 1)
    .then( res => console.log(res))
}

function insertRelationInGroupTiti(){
  const titi = userID("titi")
  const jerry = userID("jerry")

  return Promise.all([titi, jerry]).then( res => {
    const ownerID = res[0]
    const targetID = res[1]

    return knex("group_has_relationships").insert({ 
      relationships_user_id1: targetID,
      relationships_user_id2: ownerID,
      group_id: 1,
      group_user_id: ownerID
    })
  })
}

module.exports = {
  knexSeed,
  userID,
  usersName,
  passwordOfUser,
  passwordById,
  configsOfUser,
  configById,
  contentToken,
  loginOfUser,
  relationsOfUser,
  groupsOfUser,
  groupOfUser,
  insertRelationInGroupTiti
}