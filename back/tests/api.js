const chai = require("chai")
  chaiHttp = require("chai-http"),
  chaiAsPromised = require("chai-as-promised")

const { app } = require("../app"),
  { contentToken } = require("./handlers/queries"),
  { sign } = require("../handlers/webToken"),
  v = "/v" + process.env.VERSION

chai.use(chaiHttp)
chai.use(chaiAsPromised)

const Sender = function( route ){
  const route_m = route
  let url_m = route
  let set_m = {}
  let body_m = {}
  let token_m = Promise.resolve( false )

  function send(http_method){
    return token_m.then( () => chai.request(app)[http_method](v + url_m).set(set_m).send(body_m) )
  }

  function set(){
    if(typeof arguments[0] === "object" && arguments[0] !== null) set_m = Object.assign(set_m, arguments[0])
    if(
      typeof arguments[0] === "string" && 
      arguments.length === 2 && 
      typeof arguments[1] === "string"
    ) set_m[arguments[0]] = arguments[1]
    return this
  }

  function body(){
    if(arguments[0] == null) return this
    if(typeof arguments[0] !== "object") throw new Error("body take an object")
    body_m = arguments[0]
    return this
  }

  function replaceUrl(params){
    if(params == null) return this
    if(typeof params !== "object") throw new Error("params take an object")
    let newRoute = route_m
    Object.keys(params).forEach( key => (newRoute = newRoute.replace(new RegExp(key, "g") , params[key])) )
    url_m = newRoute
    return this
  }

  function tokenFor(userName){
    token_m = contentToken(userName)
      .then( content => sign(content[0]))
      .then( res => { 
        set("authorization", "bearer " + res)
        return res
      })
    return this
  }

  const methods = {
    get: () => send("get"),
    post: () => send("post"),
    put: () => send("put"),
    delete: () => send("del"),
    getToken: () => token_m,
    tokenFor,
    set,
    body,
    replaceUrl,
  }

  return Object.assign(this, methods)
}

module.exports = {
  expect: chai.expect,
  Sender
}