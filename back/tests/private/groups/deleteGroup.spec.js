const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - DELETE /group/:groupID", () => {
  const sender = new Sender("/group/:groupID")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "jerry"

    const test = senderFor(myUser).replaceUrl({":groupID": 1}).delete()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })
      
    expect(test).eventually.notify(done)
  })

  it("should remove jerry's group", (done) => {
    const myUser = "jerry"

    const test = queries.groupsOfUser(myUser).then( beforeGroups => senderFor(myUser).replaceUrl({":groupID": 1}).delete().then( () => beforeGroups) )
      .then( beforeGroups => queries.groupsOfUser(myUser).then(currentGroups => ({ beforeGroups, currentGroups })) )
      .then( res => {
        expect(res.beforeGroups).lengthOf(1)
        expect(res.currentGroups).lengthOf(0)
      })

    expect(test).eventually.notify(done)
  })
})




