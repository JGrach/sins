const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - POST /group/:groupID/relation/:relationID", () => {
  const sender = new Sender("/group/:groupID/relation/:relationID")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "titi"
    const myRelation = "jerry"

    const test = queries.userID(myRelation)
      .then( targetId => senderFor(myUser).replaceUrl({":groupID": 1, ":relationID": targetId}).post() )
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should add titi's relation with jerry in titi's group", (done) => {
    const myUser = "titi"
    const myRelation = "jerry"

    const beforeGroup = queries.groupOfUser("titi", 1)
    const relation = queries.userID(myRelation)

    const test = Promise.all([beforeGroup, relation])
      .then( res => ({ beforeGroup: res[0], targetId: res[1]}))
      .then( res => senderFor(myUser).replaceUrl({":groupID": 1, ":relationID": res.targetId}).post().then( () => res.beforeGroup ) )
      .then( beforeGroup => queries.groupOfUser("titi", 1).then( res => ({ currentGroup: res, beforeGroup })) )
      .then( res => {
        expect(res.beforeGroup).lengthOf(1)
        expect(res.currentGroup).lengthOf(1)

        expect(res.currentGroup[0].ownerID).equal(res.beforeGroup[0].ownerID)
        expect(res.currentGroup[0].groupID).equal(res.beforeGroup[0].groupID)

        expect(res.beforeGroup[0].relationLogin).equal(null)
        expect(res.currentGroup[0].relationLogin).equal(myRelation)
      })

    expect(test).eventually.notify(done)
  })
})




