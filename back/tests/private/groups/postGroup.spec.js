const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - POST /group", () => {
  const sender = new Sender("/group")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"
    const newGroup = {
      name: "Tom's Group"
    }

    const test = senderFor(myUser).body(newGroup).post()
        .then( success )
        .then( res => {
          expect(Object.keys(res.body)).lengthOf(1)
        })

    expect(test).eventually.notify(done)
  })

  it("should add tom's group with name Tom's Group", (done) => {
    const myUser = "tom"
    const newGroup = {
      name: "Tom's Group"
    }

    const test = queries.groupsOfUser(myUser)
      .then( beforeGroups => senderFor(myUser).body(newGroup).post().then( () => beforeGroups ) )
      .then( beforeGroups => queries.groupsOfUser(myUser).then(currentGroups => ({ beforeGroups, currentGroups }) ) )
      .then( res => {
        expect(res.beforeGroups).lengthOf(0)
        expect(res.currentGroups).lengthOf(1)

        const currentGroups = res.currentGroups[0]
        expect(currentGroups).a("object")
        expect(currentGroups).keys("user_id", "id", "name", "created_at", "updated_at", "picto")
        
        expect(currentGroups.user_id).a("number")
        expect(currentGroups.id).a("number")
        expect(currentGroups.name).equal("Tom's Group")
        expect(currentGroups.picto).equal("img/picto/generic/groupPicto.jpg")
      })

    expect(test).eventually.notify(done)
  })
})




