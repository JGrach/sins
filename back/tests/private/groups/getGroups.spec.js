const { Sender, expect } = require("../../api"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - GET /groups", () => {
  const sender = new Sender("/groups")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "jerry"

    const test = senderFor(myUser).get()
    .then( success )

    expect(test).eventually.notify(done)
  })

  it("should return owner's groups", (done) => {
    const myUser = "jerry"

    const test = senderFor(myUser).get()
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("groups")
        expect(res.body.groups).a("array").lengthOf(1)

        const groups = res.body.groups[0]
        expect(groups).a("object")
        expect(groups).keys("id", "name", "created_at", "updated_at", "picto")
        
        expect(groups.id).a("number")
        expect(groups.name).equal("Jerry's Group")
        expect(groups.picto).equal("img/picto/generic/groupPicto.jpg")
      })

    expect(test).eventually.notify(done)
  })
})