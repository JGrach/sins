const { Sender, expect } = require("../../api"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - GET /group/:groupID", () => {
  const sender = new Sender("/group/:groupID")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "jerry"

    const test = senderFor(myUser).replaceUrl({":groupID": 1}).get()
    .then( success )

    expect(test).eventually.notify(done)
  })

  it("should return jerry's group of id 1", (done) => {
    const myUser = "jerry"
    
    const test = senderFor(myUser).replaceUrl({":groupID": 1}).get()
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("group")
        expect(res.body.group).a("array").lengthOf(1)

        const group = res.body.group[0]
        expect(group).a("object")
        expect(group).keys("groupID", "name", "created_at", "updated_at", "groupPicto", "relations")

        expect(group.groupID).a("number")
        expect(group.name).equal("Jerry's Group")
        expect(group.groupPicto).equal("img/picto/generic/groupPicto.jpg")

        expect(group.relations).a("array")
        expect(group.relations).lengthOf(1)
        expect(group.relations[0]).a("object")
        expect(group.relations[0]).keys("relationID", "relationLogin", "relationPicto")

        expect(group.relations[0].relationID).a("number")
        expect(group.relations[0].relationLogin).equal("titi")
        expect(group.relations[0].relationPicto).equal("img/picto/generic/profilPicto.jpg")
      })

    expect(test).eventually.notify(done)
  })
})