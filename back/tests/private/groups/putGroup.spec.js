const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Groups - PUT /group/:groupID", () => {
  const sender = new Sender("/group/:groupID")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "jerry"
    const changeGroup = {
      name: "AntiCats Group"
    }

    const test = senderFor(myUser).replaceUrl({":groupID": 1}).body(changeGroup).put()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should change jerry's group 1 with name AntiCats Group", (done) => {
    const myUser = "jerry"
    const changeGroup = {
      name: "AntiCats Group"
    }

    const test = queries.groupsOfUser(myUser)
      .then( beforeGroups => senderFor(myUser).replaceUrl({":groupID": 1}).body(changeGroup).put().then( () => beforeGroups ) )
      .then( beforeGroups => queries.groupsOfUser(myUser).then(currentGroups => ({ beforeGroups, currentGroups }) ) )
      .then( res => {
        const currentGroups = res.currentGroups[0]
        const beforeGroups = res.beforeGroups[0]
        expect(currentGroups.groupOwner).equal(beforeGroups.groupOwner)
        expect(currentGroups.groupID).equal(beforeGroups.groupID)

        expect(beforeGroups.name).not.equal(changeGroup.name)
        expect(currentGroups.name).equal(changeGroup.name)
      })

    expect(test).eventually.notify(done)
  })
})




