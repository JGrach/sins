const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Users - PUT /user/config", () => {
  const sender = new Sender("/user/config")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"
    const changePicto = "img/picto/uploads/hello.jpg"
    
    const test = senderFor(myUser).body({ picto: changePicto }).put()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })
    
    expect(test).eventually.notify(done)
  })

  it("should change picto", (done) => {
    const myUser = "tom"
    const changePicto = "img/picto/uploads/hello.jpg"

    const test = queries.configsOfUser(myUser)
      .then( configBefore => configBefore[0] )
      .then( res => senderFor(myUser).body({picto: changePicto}).put().then( () => res ) )
      .then( configBefore => queries.configById( configBefore.user_id ).then( configCurrent => ({configBefore, configCurrent})) )
      .then( res => {
        expect(res.configBefore.picto).not.equal(changePicto)
        expect(res.configCurrent[0].picto).equal(changePicto)
        expect(res.configCurrent[0].user_id).equal(res.configBefore.user_id)
      })

    expect(test).eventually.notify(done)
  })

  it("should'nt change picto", (done) => {
    const myUser = "tom"
    const changePicto = "helloworld"

    const test = senderFor(myUser).body({ picto: changePicto }).put()
      .then( () => queries.configsOfUser(myUser))
      .then( configCurrent => {
        expect(configCurrent[0].picto).not.equal(changePicto)
      })

    expect(test).eventually.notify(done)
  })

  it("shouldn't change isAdmin", (done) => {
    const myUser = "jerry"
    const changeIsAdmin = true

    const test = senderFor(myUser).body({ isAdmin: changeIsAdmin }).put()
      .then( () => queries.configsOfUser(myUser))
      .then( configCurrent => {
        expect(configCurrent[0].isAdmin).equal(!changeIsAdmin)
      })

    expect(test).eventually.notify(done)
  })

  it("should change isAdmin", (done) => {
    const myUser = "tom"
    const changeIsAdmin = false

    const test = queries.configsOfUser(myUser)
      .then( configBefore => configBefore[0] )
      .then( res => senderFor(myUser).body({ isAdmin: changeIsAdmin }).put().then( () => res ) )
      .then( configBefore => queries.configById( configBefore.user_id ).then( configCurrent => ({configBefore, configCurrent})) )
      .then( res => {
        expect(res.configBefore.isAdmin).equal(!changeIsAdmin)
        expect(res.configCurrent[0].isAdmin).equal(changeIsAdmin)
        expect(res.configCurrent[0].user_id).equal(res.configBefore.user_id)
      })

    expect(test).eventually.notify(done)
  })

  it("should change isAdmin and picto", (done) => {
    const myUser = "tom"
    const changeIsAdmin = false
    const changePicto = "img/picto/uploads/hello.jpg"

    const test = queries.configsOfUser(myUser)
      .then( configBefore => configBefore[0] )
      .then( res => senderFor(myUser).body({ isAdmin: changeIsAdmin, picto: changePicto }).put().then( () => res ) )
      .then( configBefore => queries.configById( configBefore.user_id ).then( configCurrent => ({configBefore, configCurrent})) )
      .then( res => {
        expect(res.configBefore.isAdmin).equal(!changeIsAdmin)
        expect(res.configCurrent[0].isAdmin).equal(changeIsAdmin)
        expect(res.configBefore.picto).not.equal(changePicto)
        expect(res.configCurrent[0].picto).equal(changePicto)
        expect(res.configCurrent[0].user_id).equal(res.configBefore.user_id)
      })

    expect(test).eventually.notify(done)
  })

  it("shouldn't change isAdmin and picto", (done) => {
    const myUser = "jerry"
    const changeIsAdmin = true
    const changePicto = "img/picto/uploads/hello.jpg"

    const test = queries.configsOfUser(myUser)
      .then( configBefore => configBefore[0] )
      .then( res => senderFor(myUser).body({ isAdmin: changeIsAdmin, picto: changePicto }).put().then( () => res ) )
      .then( configBefore => queries.configById( configBefore.user_id ).then( configCurrent => ({configBefore, configCurrent})) )
      .then( res => {
        expect(res.configBefore.isAdmin).equal(!changeIsAdmin)
        expect(res.configCurrent[0].isAdmin).equal(!changeIsAdmin)
        expect(res.configBefore.picto).not.equal(changePicto)
        expect(res.configCurrent[0].picto).not.equal(changePicto)
        expect(res.configCurrent[0].user_id).equal(res.configBefore.user_id)
      })

    expect(test).eventually.notify(done)
  })
})