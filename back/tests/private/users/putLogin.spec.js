const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { verify } = require("../../../handlers/webToken"),
  { compare } = require("../../../handlers/bcrypt"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Users - PUT /user", () => {
  const sender = new Sender("/user")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"
    const changeLogin = "grosminet"
    const changePassword = "456"
   
    const test = senderFor(myUser).body({ login: changeLogin, password: changePassword }).put()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("token")
        const token = res.body.token
        expect(token).a("string")
        return verify("bearer " + token).then( user => {
          expect(Object.keys(user)).includes("id", "login", "isAdmin")
          expect(user.id).a("number")
          expect(user.login).equal(changeLogin)
          expect(user.isAdmin).equal(true)
        })
      })

    expect(test).eventually.notify(done)
  })

  it("should modify username", (done) => {
    const myUser = "tom"
    const myPassword = "123"
    const changeLogin = "grosminet"

    const test = queries.loginOfUser(myUser)
      .then( loginBefore => senderFor(myUser).body({ login: changeLogin, password: myPassword }).put().then(() => loginBefore) )
      .then((loginBefore) => queries.loginOfUser(changeLogin).then( login => ({ loginBefore, loginCurrent: login }) ))
      .then( res => {
        expect(res.loginBefore.login).not.equal(changeLogin)
        expect(res.loginCurrent.login).equal(changeLogin)
        expect(res.loginCurrent.id).equal(res.loginBefore.id)
      })

    expect(test).eventually.notify(done)
  })
    
  it("should modify password", (done) => {
    const myUser = "tom"
    const changePassword = "456"

    const test = queries.loginOfUser(myUser)
      .then( loginBefore => senderFor(myUser).body({ login: myUser, password: changePassword }).put().then(() => loginBefore) )
      .then((loginBefore) => queries.loginOfUser(myUser).then( login => ({ loginBefore, loginCurrent: login }) ))
      .then( res => {
        expect(res.loginCurrent.id).equal(res.loginBefore.id)

        const testOldByChangePass = compare(changePassword, res.loginBefore.password)
        const testNewByChangePass = compare(changePassword, res.loginCurrent.password)
        
        return Promise.all([testOldByChangePass, testNewByChangePass])
      })

    expect(test).eventually.members([false, true]).notify(done)
  })

  it("should modify username and password", (done) => {
    const myUser = "tom"
    const changeLogin = "grosminet"
    const changePassword = "456"

    const test = queries.loginOfUser(myUser)
      .then( loginBefore => senderFor(myUser).body({ login: changeLogin, password: changePassword }).put().then(() => loginBefore) )
      .then((loginBefore) => queries.loginOfUser(changeLogin).then( login => ({ loginBefore, loginCurrent: login }) ))
      .then( res => {
        expect(res.loginCurrent.id).equal(res.loginBefore.id)

        expect(res.loginBefore.login).not.equal(changeLogin)
        expect(res.loginCurrent.login).equal(changeLogin)

        const testOldByChangePass = compare(changePassword, res.loginBefore.password)
        const testNewByChangePass = compare(changePassword, res.loginCurrent.password)
        
        return Promise.all([testOldByChangePass, testNewByChangePass])
      })

    expect(test).eventually.members([false, true]).notify(done)
  })
})
