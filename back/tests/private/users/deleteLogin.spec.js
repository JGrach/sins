const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { verify } = require("../../../handlers/webToken"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Users - DELETE /user", () => {
  const sender = new Sender("/user")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {  
    const myUser = "tom"
    const test = senderFor(myUser).delete()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should blacklist tom's token", (done) => {
    const myUser = "tom"
    const test = senderFor(myUser).delete()
      .then (() => sender.getToken() )
      .then( token => {
        return expect(verify("bearer " + token)).eventually.rejectedWith("Token blacklisted")
      })
    
    expect(test).eventually.notify(done)
  })

  it("should delete tom's User", (done) => {
    const myUser = "tom"
    const test = expect(queries.usersName()).eventually.members(["tom", "jerry", "titi"])
      .then( () => senderFor(myUser).delete() )
      .then(() => {
        return expect(queries.usersName()).eventually.members(["jerry", "titi"])
      })

    expect(test).eventually.notify(done)
  })

  it("should delete tom's password", (done) => {
    const myUser = "tom"
    const test = queries.loginOfUser(myUser)
      .then( (loginBefore) => senderFor(myUser).delete().then(() => loginBefore) )
      .then((loginBefore) => {
        const pass = queries.passwordById(loginBefore.id)
        return expect(pass).eventually.lengthOf(0)
      })

    expect(test).eventually.notify(done)
  })

  it("should delete tom's config", (done) => {
    const myUser = "tom"
    const test = queries.loginOfUser(myUser)
      .then( (loginBefore) => senderFor(myUser).delete().then( () => loginBefore ) )
      .then((loginBefore) => {
        const config = queries.configById(loginBefore.id)
        return expect(config).eventually.lengthOf(0)
      })

    expect(test).eventually.notify(done)
  })
})