const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Users - GET /user/config", () => {
  const sender = new Sender("/user/config")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"

    const test = senderFor(myUser).get()
      .then( success )
    
    expect(test).eventually.notify(done)
  })

  it("should return owner's config", (done) => {
    const myUser = "tom"

    const test = senderFor(myUser).get()
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(2)
        expect(Object.keys(res.body)).includes("config")
        expect(res.body.config).a("array").lengthOf(1)

        const config = res.body.config[0]
        expect(config).a("object")
        expect(config).keys("user_id", "isAdmin", "picto")
        
        expect(config.user_id).a("number")
        expect(config.isAdmin).equal(true)
        expect(config.picto).equal("img/picto/generic/profilPicto.jpg")
      })

    expect(test).eventually.notify(done)
  })
})