const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Relations - POST /relations/:userIDTarget", () => {
  const sender = new Sender("/relations/:userIDTarget")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"
    const myRelation = "titi"

    const test = queries.userID(myRelation)
      .then( targetId => senderFor(myUser).replaceUrl({ ":userIDTarget": targetId }).post() )
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should add tom's relation with titi", (done) => {
    const myUser = "tom"
    const myRelation = "titi"

    const test = queries.relationsOfUser(myUser)
      .then( beforeRelations => queries.userID(myRelation).then( targetId => ({ targetId, beforeRelations })) )
      .then( res => senderFor(myUser).replaceUrl({ ":userIDTarget": res.targetId }).post().then( () => res.beforeRelations ) )
      .then( beforeRelations => queries.relationsOfUser(myUser).then(currentRelations => ({ beforeRelations, currentRelations }) ) )
      .then( res => {
        expect(res.beforeRelations).deep.members([ { login: "jerry" } ])
        expect(res.currentRelations).deep.members([ { login: "jerry" }, { login: "titi" } ])
      })

    expect(test).eventually.notify(done)
  })
})




