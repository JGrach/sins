const { Sender, expect } = require("../../api"),
  queries = require("../../handlers/queries"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Relations - DELETE /relations/:userIDTarget", () => {
  const sender = new Sender("/relations/:userIDTarget")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"
    const myRelation = "jerry"

    const test = queries.userID(myRelation)
      .then( targetId => senderFor(myUser).replaceUrl({":userIDTarget": targetId}).delete() )
      .then( success )
      .then( res => {
        expect(Object.keys(res.body)).lengthOf(1)
      })

    expect(test).eventually.notify(done)
  })

  it("should remove tom's relation with jerry", (done) => {
    const myUser = "tom"
    const myRelation = "jerry"

    const userID = queries.userID(myRelation)
    const relationsOfUser = queries.relationsOfUser(myUser)

    const test = Promise.all([userID, relationsOfUser])
      .then( (res) => ({targetId: res[0], beforeRelations: res[1]}))
      .then( res => senderFor(myUser).replaceUrl({":userIDTarget": res.targetId}).delete().then( () => res.beforeRelations) )
      .then( beforeRelations => queries.relationsOfUser(myUser).then(currentRelations => ({ beforeRelations, currentRelations }) ) )
      .then( res => {
        expect(res.beforeRelations).deep.members([ { login: "jerry" } ])
        expect(res.currentRelations).lengthOf(0)
      })

    expect(test).eventually.notify(done)
  })
})




