const { Sender, expect } = require("../../api"),
  { success } = require("../../handlers/genericResponse")

describe("Private Routes - Relations - GET /relations/", () => {
  const sender = new Sender("/relations")
  const senderFor = (user) => sender.tokenFor(user)

  it("should return success", (done) => {
    const myUser = "tom"

    const test = senderFor(myUser).get()
      .then( success )

    expect(test).eventually.notify(done)
  })

  it("should return owner's relationships", (done) => {
    const myUser = "tom"

    const test = senderFor(myUser).get()
    .then( res => {
      expect(Object.keys(res.body)).lengthOf(2)
      expect(Object.keys(res.body)).includes("relations")
      expect(res.body.relations).a("array").lengthOf(1)

      const relations = res.body.relations[0]
      expect(relations).a("object")
      expect(relations).keys("relationID", "relationLogin", "relationPicto", "groups")
      
      expect(relations.relationID).a("number")
      expect(relations.relationLogin).equal("jerry")
      expect(relations.relationPicto).equal("img/picto/generic/profilPicto.jpg")

      expect(relations.groups).a("array")
      expect(relations.groups).lengthOf(0)
    })

    expect(test).eventually.notify(done)
  })
})