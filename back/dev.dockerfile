FROM node:9.11.2
EXPOSE $BACK_PORT
COPY package.json /app/
# RUN cd /app/ && npm install --quiet
WORKDIR /app/

ENTRYPOINT ["npm", "run"]
CMD [ "dev" ]
