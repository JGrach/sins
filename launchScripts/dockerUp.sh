#!/bin/bash

# set ifs configuration
IFS_ORIGIN=$IFS
IFS_VALUE=$'='

# set arguments available
ARGS=("development" "test" "stage" "production")

# set path needed
ROOT_DIRECTORY=$( cd "$(dirname "${BASH_SOURCE[0]}")/.." ; pwd -P )
ENV_FILE="$ROOT_DIRECTORY/.env"
CREATE_ENV_SCRIPT="$ROOT_DIRECTORY/launchScripts/createEnv.sh"

# set input node env
nodeEnvInput=$1

# declare current node env information
nodeEnvCurrent=""

# verification env file 
if [ ! -f $ENV_FILE ]; then
  echo "env file doesn't exist, script will create a new .env"
  default=(NODE_ENV=development NETWORK_DOCKER=sins_network BACK_PORT=8000 REALTIME_PORT=3000 POSTGRES_PORT=1233 REDIS_PORT=6379 REDIS_PASS=)
  for d in ${default[@]}; do
    echo $d >> $ENV_FILE
  done
fi

# read NODE_ENV in env file, set current
for line in $(cat $ENV_FILE); do
  if [[ "$line" == NODE_ENV=* ]]; then 
    IFS=$IFS_VALUE read -ra nodeEnvCurrent <<< "$line"
    nodeEnvCurrent=${nodeEnvCurrent[1]}
    IFS=$IFS_ORIGIN
    break;
  fi
done

# test if NODE_ENV has been remove by hand and correct it
if [ -z "$nodeEnvCurrent" ]; then
  nodeEnvCurrent="development"
  echo "NODE_ENV=$nodeEnvCurrent" >> $ENV_FILE
fi

# if nodeEnvInput shouldn't be treated execute docker
if [ -z "$nodeEnvInput" -o "$nodeEnvInput" == "$nodeEnvCurrent" ]; then 
  echo "docker-compose -f "$ROOT_DIRECTORY/docker-compose.yml" -f "$ROOT_DIRECTORY/docker-compose.$nodeEnvCurrent.yml" up -d --build"
  docker-compose -f "$ROOT_DIRECTORY/docker-compose.yml" -f "$ROOT_DIRECTORY/docker-compose.$nodeEnvCurrent.yml" up -d --build
  exit 0
fi

# test if nodeEnvInput is correctly asked
for i in ${ARGS[@]}; do
  if [ "$1" == "$i" ]; then nodeEnvInput=$i; fi; 
done

if [ -z "$nodeEnvInput" ]; then 
  echo ERROR: "NODE_ENV must be equal to development, test, stage or production"
  exit 1
fi

# no problem override line .env and execute docker
sed -i "s/NODE_ENV=$nodeEnvCurrent/NODE_ENV=$nodeEnvInput/" $ENV_FILE
echo "docker-compose down "
docker-compose down
echo "docker-compose -f "$ROOT_DIRECTORY/docker-compose.yml" -f "$ROOT_DIRECTORY/docker-compose.$nodeEnvInput.yml" up -d --build"
docker-compose -f "$ROOT_DIRECTORY/docker-compose.yml" -f "$ROOT_DIRECTORY/docker-compose.$nodeEnvInput.yml" up -d --build
exit 0