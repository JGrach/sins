ROOT_DIRECTORY=$( cd "$(dirname "${BASH_SOURCE[0]}")/.." ; pwd -P )

createDbEnv(){
  local path=$1
  if [ ! -f  $path ]; then
    local default=(POSTGRES_DB= POSTGRES_USER= POSTGRES_PASSWORD= FIRST_USER= FIRST_USER_PASS=)
    for d in ${default[@]}; do
      echo $d >> $path
    done
  fi
}

createFilesEnv(){
  local files=("back.env" "front.env" "jwt.env" "realtime.env")
  local envs=("development" "test" "stage" "production")
  for e in ${envs[@]}; do
    $(createDbEnv "$ROOT_DIRECTORY/env/$e/db.env")
    for f in ${files[@]}; do
      if [ ! -f $ROOT_DIRECTORY/env/$e/$f ]; then
        touch $ROOT_DIRECTORY/env/$e/$f
      fi
    done
  done
}

createRepoByEnv(){
  local envs=("development" "test" "stage" "production")
  for e in ${envs[@]}; do
    if [ ! -d $ROOT_DIRECTORY/env/$e ]; then
      mkdir $ROOT_DIRECTORY/env/$e      
    fi
  done
}

createMainEnv(){
  if [ ! -f  $ROOT_DIRECTORY/.env ]; then
    local default=(NODE_ENV=development NETWORK_DOCKER=sins_network BACK_PORT=8000 REALTIME_PORT=3000 POSTGRES_PORT=1233 REDIS_PORT=6379 REDIS_PASS=)
    for d in ${default[@]}; do
      echo $d >> $ROOT_DIRECTORY/.env
    done
  fi
}

createRepoEnv(){
  if [ ! -d $ROOT_DIRECTORY/env ]; then
    mkdir $ROOT_DIRECTORY/env
  fi
}

$(createMainEnv)

