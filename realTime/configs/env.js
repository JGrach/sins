const env = process.env.NODE_ENV || (process.env.NODE_ENV = "development")

function shouldKill(){
  const haveToFilled = [
    "VERSION",
    "REALTIME_PORT",
    "JWT_SECRET",
    "JWT_EXPIRATION",
    "REDIS_HOST",
    "REDIS_PORT",
    "REDIS_PASS"
  ]

  const notFilled = haveToFilled.filter( needed => {
    if(!process.env[needed]) console.error("\x1b[31m", needed + " is an environment variable and must be initialized", "\x1b[0m")
    return !process.env[needed]
  })

  if(notFilled.length > 0) process.exit(1)
}

if(env == "development" || env == "test" && process.env.INIT != "true"){

  function loadDotenvFile(){
    const fs = require("fs"),
      { promisify } = require("util"),
      dotenv = require("dotenv")
    const envFiles = [
      __dirname + "/.env",
      __dirname + "/../../env/development/realtime.env",
      __dirname + "/../../env/development/jwt.env",
    ]

    function envExist(envPath){
      try{
        fs.accessSync(envPath)
        return true
      } catch(err) {
        return false
      }
    }

    envFiles.forEach( envFile => {
      if(envExist(envFile)) dotenv.config({ path: envFile })
    })
  }

  function settingDefault(){
    const devDefault = {
      VERSION: 0,
      REDIS_HOST: "localhost",
      REDIS_PORT: 6379,
      REALTIME_PORT: 8000,
      JWT_SECRET: "test",
      JWT_EXPIRATION: "7d",
      REDIS_PASS: "whatever"
    }
  
    Object.keys(devDefault).forEach(key => {
      if(!process.env[key]){
        process.env[key] = devDefault[key]
        console.warn("\x1b[33m", "Env variable " + key + " no detected, set to " + devDefault[key], "\x1b[0m")
      }
    })  
  }

  loadDotenvFile()
  settingDefault()

  process.env.INIT = "true"
}

shouldKill()

module.exports = env